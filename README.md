# sphinx_prebuild

An extension using the prebuilder only has to override the generate and optionally name function of the parent class 


This example is a complete extension! (minus the .j2 template)

```python
from pathlib import Path
from sphinx_prebuild import PreBuilder
from oscal_parser import OSCAL_FILE

class OscalPreBuilder(PreBuilder):

    def name(self, docname):
        return self.documents[docname].name

    def generate(self):
        oscal_roots = self.app.config.oscal_roots
        for root in oscal_roots:
            oscal_root = self.source_root / Path(root)
            for file in oscal_root.rglob('*.yaml'):
                data = OSCAL_FILE.from_file(file)
                self.render_template('cdef.j2', data)

def setup(app):
    builder = OscalPreBuilder(app)
    app.add_config_value('oscal_roots', [], 'env')
```