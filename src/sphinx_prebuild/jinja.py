from jinja2 import FileSystemLoader, Environment
from pathlib import Path 
import yaml

class Jinja:
    def __init__(self, root=None):
        if not root:
            root = Path(__name__).parent        
        self.env = Environment(loader=FileSystemLoader(searchpath=root))
        self.set_filters()
          

    def set_filters(self):
        for func in dir(self):
            if callable(getattr(self, func)) and func.startswith('filter_'):
                self.env.filters[func.replace('filter_','')] = getattr(self, func)
    def add_filter(self, name, func):
        self.env.filters[name] = func

    def filter_rstTitle(self, name, level='='):
        if isinstance(level, int):
            if level == 1:
                level = '='
            elif level == 2:
                level = '-'
            elif level == 3:
                level = '"'
            else:
                level = '^'
        return f"{name}\n{level*len(name)}"
    
    def filter_toYaml(self, content):
        return yaml.safe_dump(content)

    def render_template(self, template_name, data={}):
        template = self.env.get_template(template_name)
        if isinstance(data, dict):
            return template.render(**data)
        else:
            return template.render(data=data)
        
