from pathlib import Path
import os
import random  
import string
import re
from shutil import rmtree
from tempfile import NamedTemporaryFile
from importlib import import_module
from .jinja import Jinja

class PreBuilder:
    def __init__(self, app):
        self.app = app
        app.connect('config-inited', self._config_init)
        app.connect('builder-inited', self.build_init)
        app.connect('build-finished', self.cleanup)
        app.add_config_value('prebuild_index', True, 'env')
        app.add_config_value('prebuild_seperatepdfs', True, 'env')

        self.module = self.__module__
        caller = import_module(self.__module__)
        self.jinja = Jinja(
            root=Path(caller.__file__).parent/'assets'
        )
        self.jinja.add_filter('name2file',self.name2file)

        self.tmpdir = None
        self.source_root = Path(os.environ.get('SOURCEDIR', app.srcdir))
        self.output_dir = None
        self.documents = {}
        
    def config_init(self):
        pass 

    def _config_init(self, *args, **kwargs):
        self.config_init()
        self.prepare_dir()
        

    def build_init(self, *args, **kwargs):
        self.generate()
        if self.app.config.prebuild_index: self.gen_index()
        self.update_latex(self.app.config)

    def prepare_dir(self):
        self.output_dir = self.source_root / self.__module__
        if self.output_dir.exists():
            rmtree(self.output_dir)
        self.output_dir.mkdir()

    def name(self, data):
        return ''.join(random.choices(string.ascii_lowercase, k=8))
    
    def name2file(self, name):
        return re.sub(r'[\s\:\.]','_',str(name)).lower()
        # return str(name).replace(' ','_').replace(':','_').replace('.','_').replace('_','').lower()
    
    def generate(self, *args, **kwargs):
        pass

    def cleanup(self, *args, **kwargs):
        # rmtree(self.output_dir)
        return

    def render_template(self, template_name, data={}, extension='rst', filename=None, output_dir=None):
        txt = self.jinja.render_template(template_name, data)
        docname = self.name(data)
        if filename:
            docfile = self.name2file(filename)
        else:
            docfile = self.name2file(docname)

        if output_dir:
            outdir = Path(self.name2file(output_dir))
            Path(outdir).mkdir(parents=True,exist_ok=True)
        else:
            outdir = self.output_dir
        with open(str(outdir/docfile)+f".{extension}", 'w') as f:
            f.write(txt)
        outname = outdir.relative_to(self.output_dir)/docfile
        self.documents[outname] = data
        return docfile

    def gen_index(self):
        j2 = Jinja(root=Path(__file__).parent/'assets')
        with open(self.output_dir / 'index.rst', 'w') as f:
            f.write(j2.render_template(
                        'index.j2', 
                        dict(name=self.module,files=self.documents.keys())
                    ))
            
    def update_latex(self, config):
        if config.prebuild_seperatepdfs:
            for docname in self.documents:
                docroot = self.output_dir.relative_to(self.source_root)
                docfile = docname
                docdata = self.documents.get(docfile)
                doctitle = self.name(docdata)
                doc_path =  docroot / docname
                config.latex_documents += [(
                    doc_path.as_posix(),
                    f"{docname.stem}.tex",
                    doctitle,
                    '',
                    'manual',
                    False
                )]
            # print(f"??? {docfile} {config.latex_documents[-1]}")
